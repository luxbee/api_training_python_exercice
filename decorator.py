def full_name(first_name, last_name):
    return first_name + " " + last_name

def list_names(func):
    def inner(str_couples):
        return "\n".join([func(val[0], val[1]) for val in str_couples])
    return inner

full_name = list_names(full_name)

players_list = [("lionel", "Messi"), ("sergio", "Ramos")]

# print (full_name("1", "2"))

print(full_name(players_list))
