from pydantic import BaseModel, ValidationError
from typing import Optional, Dict, List

import sys

class Client(BaseModel):
    name : str
    age: int
    type: Optional[str]
    owners: List[str]
    area_by_room: Dict[str, float]
try:
    client = Client(name="patrick", age=55, owners=["linda", "john"], area_by_room={"office": 20.0})
except ValidationError as e:
    print(e.json())