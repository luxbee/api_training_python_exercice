import math

class Calculation():
    def __init__(self) -> None:
        pass
    
    def factorial(self, x) -> int:
        if x>1: return x*self.factorial(x-1)
        else: return 1
        
    def sum(self, n) -> int:
        if n>1: return n+self.sum(n-1)
        else: return 1
    
    def prime(self, x) -> bool:
        for i in range(2, x-1):
            if x%i==0: return False
        return True
    
    def twinprimes(self, x, y) -> bool:
        if (self.prime(x)) & (self.prime(y)) & (abs(x-y)==2): return True
        else: return False
    
    def multiplitable(self, x) -> None:
        for i in range(10): print(f"{i} * {x} = {x*i}")
        
    def tenmultiplitables(self) -> None:
        for i in range(10):
            for j in range(10):
                print(f"{i} * {j} = {i*j}")
            print()
    
    def divlist(self, x) -> None:
        for i in range(2, x-1):
            if x%i==0: print(f"{i} is a {x} divisor")
    
    def primedivlist(self, x) -> None:
        for i in range(2, x-1):
            if (x%i==0) & self.prime(i) : print(f"{i} is a prime {x} divisor")
    

myCalc = Calculation()
x=10
y=11
t=5
k=7
z=20
print (f"factorial {x} = {myCalc.factorial(x)}")
print (f"Sum of the {x} first integer= {myCalc.sum(x)}")
print (f"is {x} primenumber : {myCalc.prime(x)}")
print (f"is {y} primenumber : {myCalc.prime(y)}")
print(f"is {t} and {k} twinprimes number : {myCalc.twinprimes(t,k)}")
print(f"multiply table of :  {k}")
myCalc.multiplitable(7)
print("table multiply : ")
myCalc.tenmultiplitables()
myCalc.divlist(10)
myCalc.primedivlist(20)


    
        