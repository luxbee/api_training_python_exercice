class BankAccount:
    def __init__(self, accountNumber, accountOwner, accountBalance) -> None:
        self.accountNumber = accountNumber
        self.accountOwner= accountOwner
        self.accountBalance = accountBalance
        
    def deposit(self, amount):
        self.accountBalance += amount
    
    def withdrawal(self, amount):
        self.accountBalance -= amount
    
    def agios(self):
        self.accountBalance-=self.accountBalance*0.05
        
    def display(self):
        print(f"Account Number : {self.accountNumber} Account Owner : {self.accountOwner} Account Balance : {self.accountBalance}")
    
myBankAccount = BankAccount("N123", "Patrick PIERRA", 100)
myBankAccount.withdrawal(15)
myBankAccount.agios()
myBankAccount.display()
    
    
        