friend_pieces = {
    "Mary" : 2,
    "John" : 1,
    "Richard" : 3,
    "Lisa" : 3
}
nb_pieces = 14
sequence = ["John", "Richard", "John", "Lisa", "Mary", "Richard", "Lisa"]
i=0
while (i<len(sequence) and nb_pieces >0):
    nb_pieces = nb_pieces-friend_pieces[sequence[i]]
    i=i+1
print(f"Nbr chocolate remaining : {nb_pieces}")
