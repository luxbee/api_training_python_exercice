class flashcard:
    def __init__(self, word, meaning) -> None:
        self.word = word
        self.meaning = meaning
        
    def __str__(self) -> str:
        return self.word + " " + self.meaning

myDictionary = list()
for i in range(3):
    word = input("entrez un mot : ")
    definition = input("entrez la definition : ")
    myDictionary.append(flashcard(word=word, meaning=definition).__str__())
    
for entry in myDictionary:
    print(entry)
    