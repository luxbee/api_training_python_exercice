import math

class Point():
    def __init__(self, x, y) -> None:
        self.x = x
        self.y = y

class Circle():
    def __init__(self, center, radius) -> None:
        self.radius = radius
        self.center = center
    def surface(self) -> float:
        return math.pi*self.radius**2
    def perimeter(self) -> float:
        return 2*math.pi*self.radius
    def belonging(self, point) -> bool:
        #print ((self.center.x-point.x)**2+(self.center.y-point.y)**2-self.radius**2)
        return (self.center.x-point.x)**2+(self.center.y-point.y)**2-self.radius**2==0

myCircleCenter = Point(10,10)
myCircle = Circle(myCircleCenter, 10)
myfirstPoint= Point(10,20)
mysecondPoint = Point(-10, -10)
print(myCircle.surface())
print(myCircle.perimeter())
print(myCircle.belonging(myfirstPoint))
print(myCircle.belonging(mysecondPoint))

    