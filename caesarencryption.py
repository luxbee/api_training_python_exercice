class CaesarEncryption():
    
    def __init__(self) -> None:
        self.encryptedMessage = ''
        self.decryptedMessage =''
        
    def encryption(self, string, key) -> str:
        for i in range(len(string)):
            ascii = ord(string[i]) + key
            if (ascii > 126): ascii = ascii-126+32
            self.encryptedMessage+=chr(ascii)
            
    def decryption(self, string, key) -> str:
        for i in range(len(string)):
            ascii = ord(string[i]) - key
            if (ascii < 32): 
                ascii = 126-32-ascii
            self.decryptedMessage+=chr(ascii)
            
    def display(self) -> None:
        print(self.encryptedMessage)
        print(self.decryptedMessage)

myCaesar = CaesarEncryption()
myCaesar.encryption("Once upon a time", 10)
myCaesar.display()
myCaesar.decryption("Yxmo*!zyx*k*~swo", 10)
myCaesar.display()
