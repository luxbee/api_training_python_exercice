class Book():
    def __init__(self, title, firstname, lastname, price) -> None:
        self.title = title
        self.firstname = firstname
        self.lastname = lastname
        self.price = price
    def view(self) -> None:
        print(f"Titre : {self.title} /// author firstname : {self.firstname} /// author lastname: {self.lastname} /// price : {self.price}")

mybook = Book('one day in training', 'Pierre', 'Gillet', 100)
mybook.view()