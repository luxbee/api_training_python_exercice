from pydantic import BaseModel
import pydantic

class User(BaseModel):
    id: int
    name = 'patrick'
    
myuser= User(id=10)
print(dict(myuser))

secondUser = dict = {} 
secondUser = {'id': 20, 'name': 'laurent'}
secondUser = pydantic.parse_obj_as(User, secondUser)

print(secondUser)
