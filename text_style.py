# The decorator to make it bold
def makebold(fn):
    # The new function the decorator returns
    def wrapper():
        # Insertion of some code before and after
        return "<b>" + fn() + "</b>"
    return wrapper

# The decorator to make it italic
def makeitalic(fn):
    # The new function the decorator returns
    def inner():
        # Insertion of some code before and after
        return "<i>" + fn() + "</i>"
    return inner

@makebold
@makeitalic
def say():
    return "hello"

print(say())