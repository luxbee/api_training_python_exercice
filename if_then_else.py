a=10
if a == 2:
    print("The variable a is equal to 2")
    print("We can continue with the nested code part")
elif a < 0:
    print("negativ value")
else:
    print("just other value")

my_list = [2, "some text", 3.2, 17, "some other text"]
i = 0
while i < len(my_list):
    print(my_list[i])
    i = i + 1

for i in range(len(my_list)):
    print(i, my_list[i])

for index, value in enumerate(my_list):
    print(f"index :  {index}, value : {value}")
    
my_dictionary = {"name": "Paris",
                 "population": 1.9}
for key in my_dictionary:
    print(f"{key}: {my_dictionary[key]}")

result =[i*i for i in range(1, 11)]
print (result)