def is_palindrome(x):
    x=str(x)
    x=x.replace(" ","")
    # print (x)
    demi_longueur=len(x)//2
    for i in range(demi_longueur-1):
        if x[i]!=x[len(x)-1-i]:
            return False
    return True

x=input("entrez le mot : ")
if is_palindrome(x):
    print(f"{x} est un palindrome")
else:
    print(f"{x} n'est pas un palindrome")