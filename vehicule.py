class Vehicle:
    def __init__(self, name, max_speed, mileage) -> None:
        self.name = name
        self.max_speed = max_speed
        self.mileage = mileage

class Bus(Vehicle):
    def __init__(self, name, max_speed, mileage) -> None:
        super().__init__(name, max_speed, mileage)
        self.capacity = 50

class Motorbike(Vehicle):
    def __init__(self, name, max_speed, mileage) -> None:
        super().__init__(name, max_speed, mileage)

my_bus = Bus("The Beast", int(100), int(100000) )
print (my_bus.name, " ", my_bus.capacity, " ", my_bus.max_speed, " ", my_bus.mileage)
