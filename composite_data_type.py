my_dictionary = {"name": "Paris",
                 "population" : 1.9,
                 "country" : "France",
                 "is_capital": True}
paris_country = my_dictionary["country"]
my_dictionary["population"] = 2.1
print(my_dictionary)
print(type(my_dictionary))

my_list = (1,2,3, "toto")
print(my_list)

print(7//3)
print(3%2)

n="hello"
print(n[2:4])
print(n.upper())
sep="1_2_3"
print(sep.split())