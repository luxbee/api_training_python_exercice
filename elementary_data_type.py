# Integers
n=17
print(type(n))

#Floats
y=8.59
print(type(y))

# Strings
my_text = "This is my text"
print(type(my_text))

# Booleans
first_boolean = True
second_boolean = False
print(type(first_boolean))