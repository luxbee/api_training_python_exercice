nb_sides = int(input("Number of sides : "))
same_length = input("Same lenght : ")
diag_same_length = input("diagonals have same lenght : ")
if (same_length=="True") or (same_length=="true"):
    same_length = True
else:
    same_length = False

if (diag_same_length=="True") or (diag_same_length=="true"):
    diag_same_length = True
else:
    diag_same_length = False
    
# print (same_length)
# print (diag_same_length)
if nb_sides<4:
    if same_length:
        print("Equilateral triangle")
    else:
        print("Triangle")
elif nb_sides<5:
    if same_length:
        if diag_same_length:
            print("Square")
        else:
            print("Diamond")
    else:
        print("Rectangle")
elif nb_sides==5:
    print("Pentagon")
else:
    print("Polygon")
